# Copyright (C) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/location/config.gni")
import("//build/ohos.gni")
ohos_shared_library("lbsservice_locator") {
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  branch_protector_ret = "pac_ret"

  sources = [
    "$SUBSYSTEM_DIR/location_locator/callback/source/cached_locations_callback_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/callback/source/country_code_callback_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/callback/source/gnss_status_callback_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/callback/source/locating_required_data_callback_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/callback/source/locator_callback_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/callback/source/nmea_message_callback_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/fusion_controller.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/geo_convert_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/gnss_ability_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/location_config_manager.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/locator_ability.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/locator_background_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/locator_event_manager.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/locator_event_subscriber.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/locator_required_data_manager.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/locator_skeleton.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/network_ability_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/passive_ability_proxy.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/permission_status_change_cb.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/report_manager.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/request.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/request_manager.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/subability_common.cpp",
    "$SUBSYSTEM_DIR/location_locator/locator/source/work_record.cpp",
  ]

  include_dirs = [
    "$LOCATION_ROOT_DIR/interfaces/inner_api/include",
    "$SUBSYSTEM_DIR/location_geocode/geocode/include",
    "$SUBSYSTEM_DIR/location_gnss/gnss/include",
    "$SUBSYSTEM_DIR/location_locator/callback/include",
    "$SUBSYSTEM_DIR/location_locator/locator/include",
    "$SUBSYSTEM_DIR/location_network/network/include",
    "$SUBSYSTEM_DIR/location_passive/passive/include",
  ]

  deps = [ "$LOCATION_COMMON_DIR:lbsservice_common" ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libprivacy_sdk",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "data_share:datashare_consumer",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "init:libbegetutil",
    "ipc:ipc_single",
    "os_account:os_account_innerkits",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = []
  if (communication_wifi_enable) {
    external_deps += [ "wifi:wifi_sdk" ]
    defines += [ "WIFI_ENABLE" ]
  }

  if (communication_bluetooth_enable) {
    external_deps += [ "bluetooth:btframework" ]
    defines += [ "BLUETOOTH_ENABLE" ]
  }

  if (location_feature_with_geocode) {
    defines += [ "FEATURE_GEOCODE_SUPPORT" ]
  }

  if (location_feature_with_gnss) {
    defines += [ "FEATURE_GNSS_SUPPORT" ]
  }

  if (location_feature_with_network) {
    defines += [ "FEATURE_NETWORK_SUPPORT" ]
  }

  if (location_feature_with_passive) {
    defines += [ "FEATURE_PASSIVE_SUPPORT" ]
  }

  if (resourceschedule_background_task_mgr_enable) {
    external_deps += [ "background_task_mgr:bgtaskmgr_innerkits" ]
    defines += [ "BGTASKMGR_SUPPORT" ]
  }

  part_name = "location"
  subsystem_name = "location"
}
